<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Basic PHP7 Ex 1_7</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>\
        <<h1>Hello PHP7</h1>
        <?php
            echo "Starting with PHP7";
            echo "<h3>Hello Title H3</H3>";
        ?>
        <h3>Hello PHP7 code<?php echo "This H3"; ?></h3>
    
    </body>
</html>